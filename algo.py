import cv2
import numpy as np
from queue import Queue
from random import randint

MAX_H = 999
MAX_W = 1337
TARGET_H = 300

def determine_hydration(original):
    #print(original.shape)
    #print("1")
    image = original.copy()
    #print("2")
    mask = tree_filter(image)
    #print("3")
    mask = cv2.bitwise_and(mask, color_filter(image))
    #print("4")
    image = contrast(image)
    #print("5")
    mask_filter(image, mask)
    #print("6")
    contours, hydration = find_field(image)
    #print("7")
    new_contours = filter_size(contours, original.shape)
    #print("8")
    return parse_answer(new_contours, hydration)

def parse_answer(contours, hydration):
    print(len(contours), len(hydration))
    res = []
    for i in range(len(contours)):
        a = cv2.convexHull(contours[i], False)
        cur_points = []
        for j in range(0, len(contours[i]), 3):
            p = contours[i][j]
            cur_points.append({"x": int(p[0][0]), "y": int(p[0][1])})
        res.append({"hydration": hydration[i], "points": cur_points})
    return res

def filter_size(cnt, shape):
    hull = []
    res = []
    for i in cnt:
        hull.append(cv2.convexHull(i, False))

    for i in range(len(cnt)):
        Sc = cv2.contourArea(cnt[i])
        Sh = cv2.contourArea(hull[i])
        if (Sh / Sc < 2):
            res.append(cnt[i])
    return res

def hotfix(image):
    boundaries = (
                (np.array([0, 90, 0]), np.array([20, 255, 255])),
                (np.array([20, 90, 0]), np.array([25, 255, 255])),
                (np.array([25, 90, 0]), np.array([32, 255, 255])),
            )
    k = 0
    for (lower, upper) in boundaries:
        lower = np.array(lower, dtype = "uint8")
        upper = np.array(upper, dtype = "uint8")
        mask = cv2.inRange(image, lower, upper)
        output = cv2.bitwise_and(image, image, mask = mask)
        k += 1
        cv2.imshow(str(k), output)

def find_field(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    boundaries = (
                (np.array([0, 90, 0]), np.array([20, 255, 255])),
                (np.array([20, 90, 0]), np.array([25, 255, 255])),
                (np.array([25, 90, 0]), np.array([32, 255, 255])),
            )
   
    mask = np.zeros((image.shape[0], image.shape[1]))
    mask = np.array(mask, dtype = "uint8")
    for (lower, upper) in boundaries:
        lower = np.array(lower, dtype = "uint8")
        upper = np.array(upper, dtype = "uint8")
        cur_mask = cv2.inRange(hsv, lower, upper)
        mask = cv2.bitwise_or(mask, cur_mask)

    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if (int(image[i][j][0]) + int(image[i][j][1]) + int(image[i][j][2]) < 5):
                mask[i][j] = 0
            else:
                if (mask[i][j] != 0):
                    mask[i][j] = 255
    
    mask = np.array(mask, dtype = "uint8")
    
    contours_unfiltered, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours = []
    for i in contours_unfiltered:
        if (cv2.contourArea(i) > mask.shape[0] * mask.shape[1] / 500):
            contours.append(i)

    hydration = []
    res_contours = []
    for i in range(len(contours)):
        mask = np.zeros((len(image), len(image[0])))
        cv2.drawContours(mask, contours, i, 255, -1)
        pixelpoints = cv2.findNonZero(mask)
        r = 0
        g = 0
        b = 0
        for j in pixelpoints:
            p = j[0]
            r += int(image[p[1]][p[0]][2])
            g += int(image[p[1]][p[0]][1])
            b += int(image[p[1]][p[0]][0])
        r //= len(pixelpoints)
        g //= len(pixelpoints)
        b //= len(pixelpoints)
        h_l = hydration_level((b, g, r))
        if h_l != 3 and len(contours[i]) > 3:
            hydration.append(h_l)
            res_contours.append(contours[i])
    
    return res_contours, hydration


def hydration_level(color):
    hcolor = np.uint8([[color]])
    hsv = cv2.cvtColor(hcolor, cv2.COLOR_BGR2HSV)
    bounds = [
                (np.array([0, 0, 0]), np.array([24, 255, 250])),
                (np.array([24, 0, 0]), np.array([25, 255, 250])),
                (np.array([25, 0, 0]), np.array([32, 255, 250])),
             ]
    percent = [0, 1, 2]
    for i in range(3):
        if cv2.inRange(hsv, bounds[i][0], bounds[i][1]):
            return percent[i]
    return 3


def mask_filter(image, mask):
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if (mask[i][j] == 0):
                for l in range(3):
                    image[i][j][l] = 0

def contrast(image):
    brightness = 245
    contrast = 187
    brightness = map(brightness, 0, 510, -255, 255)
    contrast = map(contrast, 0, 254, -127, 127)
    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow
        buf = cv2.addWeighted(image, alpha_b, image, 0, gamma_b)
    else:
        buf = image.copy()
    if contrast != 0:
        f = float(131 * (contrast + 127)) / (127 * (131 - contrast))
        alpha_c = f
        gamma_c = 127*(1-f)
        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)
    return buf

def map(x, in_min, in_max, out_min, out_max):
    return int((x-in_min) * (out_max-out_min) / (in_max-in_min) + out_min)   

def color_filter(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    bounds = [
                [np.array([70,0,0]), np.array([120,255,255])],
                [np.array([10,30,130]), np.array([36,100,200])]
             ]
    mask1 = cv2.bitwise_not(cv2.inRange(hsv, bounds[1][0], bounds[1][1]))
    mask1 = size_mask_filter(mask1)
    mask1 = cv2.bitwise_not(mask1)
    mask1 = np.array(mask1, dtype = np.uint8)
    mask2 = cv2.bitwise_not(cv2.inRange(hsv, bounds[0][0], bounds[0][1]))
    res = cv2.bitwise_and(mask1, mask2)
    return res

def tree_filter(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    res = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    for x in range(2, image.shape[0] - 2):
        for y in range(2, image.shape[1] - 2):
            dist = 5
            min_v = hsv[x][y][2]
            max_v = hsv[x][y][2]
            for i in range(-(dist // 2), dist // 2 + 1, 1):
                for j in range(-(dist // 2), dist // 2 + 1, 1):
                    min_v = min(min_v, hsv[x + i][y + j][2])
                    max_v = max(max_v, hsv[x + i][y + j][2])
            res[x][y] = max_v - min_v
    ret,thresh = cv2.threshold(res,50,255,cv2.THRESH_BINARY)
    return cv2.bitwise_not(thresh)

def size_mask_filter(mask, flag=True):
    mask = cv2.bitwise_not(mask)
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(mask, connectivity=8)
    sizes = stats[1:, -1]
    nb_components = nb_components - 1
    max_size = sizes[0]
    max_i = 0
    res = np.ones(output.shape)
    if (len(sizes) == 0):
        return res
    if (flag == True):
        for i in range(0, nb_components):
            if (sizes[i] > max_size):
                max_size = sizes[i]
                max_i = i;
        res[output == max_i + 1] = 0
    else:
        for i in range(0, nb_components):
            if (sizes[i] > mask.shape[0] * mask.shape[1] / 500):
                res[output == i + 1] = 0
        res = cv2.bitwise_not(res)
    return res

if __name__ == "__main__":
    pic = cv2.imread('photo1.png', 1)
    ans, cnt = determine_hydration(pic)
    color = ((0, 0, 255), (0, 160, 255), (0, 200, 200), (200, 0, 200))
    for i in range(len(cnt)):
        cv2.drawContours(pic, cnt, i, color[int(ans[i]['hydration'])], 1, 1)
    cv2.imshow('lolkek', pic)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
