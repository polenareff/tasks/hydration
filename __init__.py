from http.server import BaseHTTPRequestHandler, HTTPServer
import cv2, json, numpy as np

from .algo import determine_hydration, MAX_H, MAX_W, TARGET_H

class Server(BaseHTTPRequestHandler):

    @classmethod
    def raw2img(cls, raw):
        arr = np.frombuffer(raw, dtype=np.uint8)
        img = cv2.imdecode(arr, cv2.IMREAD_UNCHANGED)
        if img is None:
            raise Exception('Invalid image data provided!')
        return img

    def do_POST(self):
        content_length = int(self.headers.get('Content-Length'))
        request_data = self.rfile.read(content_length)
        try:
            request_image = Server.raw2img(request_data)
            h, w = request_image.shape[:2]
            if h > MAX_H or w > MAX_W:
                raise Exception('The image provided is too large.')
            scale_factor = TARGET_H / h
            request_image = cv2.resize(request_image, (0, 0), fx=scale_factor, fy=scale_factor)
            
            response_data = determine_hydration(request_image)

            for field in response_data:
                for point in field['points']:
                    point['x'] = int(point['x'] / scale_factor)
                    point['y'] = int(point['y'] / scale_factor)
            response_json = json.dumps(response_data)
        except:
            self.send_response(400)
            self.end_headers()
        else:
            self.send_response(200)
            self.end_headers()
            self.wfile.write(response_json.encode())

httpd = HTTPServer(('0.0.0.0', 6000), Server)
httpd.serve_forever()
